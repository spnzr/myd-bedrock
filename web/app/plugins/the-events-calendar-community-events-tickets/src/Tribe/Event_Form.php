<?php

class Tribe__Events__Community__Tickets__Event_Form {
	/**
	 * @var Tribe__Events__Community__Tickets__Main $main Parent community tickets object
	 */
	public $main;

	/**
	 * Constructor!
	 */
	public function __construct( $main ) {
		$this->main = $main;

		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_resources' ) );
		add_action( 'tribe_post_get_template_part_community/modules/cost', array( $this, 'add_edit_tickets' ) );

		add_action( 'tribe_events_community_display_cost_section', array( $this, 'maybe_hide_event_cost' ), 20 );

		// we must use the update_post_metadata filter because we always want to make sure the payment split
		// is set appropriately even if the _regular_price meta value hasn't changed
		add_filter( 'update_post_metadata', array( $this, 'set_adaptive_payment_data' ), 10, 5 );

		add_filter( 'tribe_events_tickets_module_name', array( $this, 'ticket_module_name' ) );
		add_filter( 'tribe_events_tickets_report_url', array( $this, 'ticket_report_url' ), 10, 2 );
		add_filter( 'tribe_events_tickets_attendees_url', array( $this, 'ticket_attendees_url' ), 10, 2 );
		add_filter( 'tribe_events_tickets_woo_display_sku', '__return_false' );

		// make sure the user can only see images he/she has uploaded
		add_filter( 'parse_query', array( $this, 'view_only_users_media' ) );
		add_filter( 'has_cap', array( $this, 'give_subscribers_upload_files_role' ) );
	}//end __construct

	/**
	 * Enqueues needed styles and scripts
	 */
	public function enqueue_resources() {
		wp_enqueue_script( 'event-tickets' );
		wp_enqueue_script( 'events-community-tickets' );
		wp_enqueue_style( 'event-tickets' );
		wp_enqueue_style( 'events-community-tickets' );

		$nonces = array(
			'add_ticket_nonce'    => wp_create_nonce( 'add_ticket_nonce' ),
			'edit_ticket_nonce'   => wp_create_nonce( 'edit_ticket_nonce' ),
			'remove_ticket_nonce' => wp_create_nonce( 'remove_ticket_nonce' ),
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
		);

		wp_localize_script( 'event-tickets', 'TribeTickets', $nonces );
		wp_localize_script( 'event-tickets', 'tribe_ticket_notices', array(
			'confirm_alert' => esc_html__( 'Are you sure you want to delete this ticket? This cannot be undone.', 'tribe-events-community-events' ),
		) );

		Tribe__Tickets__Metabox::localize_decimal_character();

		// using the event-tickets localization here because it is a pre-requisite, so this translation should be available
		$upload_header_data = array(
			'title'  => __( 'Ticket header image', 'event-tickets' ),
			'button' => __( 'Set as ticket header', 'event-tickets' ),
		);
		wp_localize_script( 'event-tickets', 'HeaderImageData', $upload_header_data );

		wp_enqueue_media();
	}

	/**
	 * Appends the ticket UI onto the Cost section of Community Tickets
	 */
	public function add_edit_tickets() {
		if ( ! is_user_logged_in() ) {
			return;
		}

		$events = Tribe__Events__Main::instance();

		tribe_get_template_part( 'community-tickets/modules/tickets' );
	}//end add_edit_tickets

	/**
	 * Hooked to the update_post_meta action
	 */
	public function set_adaptive_payment_data( $check, $object_id, $meta_key, $meta_value, $prev_value ) {
		if ( '_regular_price' != $meta_key ) {
			return;
		}//end if

		if ( ! Tribe__Events__Community__Tickets__Main::instance()->is_split_payments_enabled() ) {
			return;
		}

		$event = get_post( $object_id );
		$user = get_user_by( 'id', $event->post_author );

		// if the owner of the event cannot create tickets, bail
		if ( ! user_can( $user, 'edit_event_tickets' ) ) {
			return;
		}

		$meta = get_user_meta( $event->post_author, Tribe__Events__Community__Tickets__Payment_Options_Form::$meta_key, true );

		if ( empty( $meta['paypal_account_email'] ) ) {
			return;
		}

		update_post_meta( $object_id, '_paypal_adaptive_receivers', $meta['paypal_account_email'] . ' | 90' );
	}//end set_adaptive_payment_data

	/**
	 * Renames the ticket module to a more generic name for the front end.
	 */
	public function ticket_module_name( $name ) {
		// using the event-tickets translation here, because that is the one that will potentially be changed
		if ( $name != __( 'RSVP', 'event-tickets' ) ) {
			$name = __( 'Tickets', 'tribe-events-community-events' );
		}
		return $name;
	}//end ticket_module_name

	/**
	 * Replaces the attendees URL on the front-end with a community-friendly URL
	 */
	public function ticket_attendees_url( $url, $event_id ) {
		$url = get_bloginfo( 'url' ) . '/' . $this->main->routes['attendees-report']->path( "/{$event_id}" );

		return $url;
	}//end ticket_attendees_url

	/**
	 * Replaces the report URL on the front-end with a community-friendly URL
	 */
	public function ticket_report_url( $url, $event_id ) {
		$url = get_bloginfo( 'url' ) . '/' . $this->main->routes['sales-report']->path( "/{$event_id}" );

		return $url;
	}//end ticket_report_url

	/**
	 * Hooked to parse_query to limit media to include ONLY media uploaded by the current user
	 *
	 * @param $wp_query WP_Query WP Query object
	 *
	 * @return WP_Query
	 */
	public function view_only_users_media( $wp_query ) {
		global $current_user;

		// if the user isn't viewing the media library, bail
		if ( false === strpos( $_SERVER['REQUEST_URI'], '/wp-admin/upload.php' ) ) {
			return $wp_query;
		}

		// if a user is a contributor or greater, don't filter the media they can see. bail.
		if ( current_user_can( 'edit_posts' ) ) {
			return $wp_query;
		}

		$user_id = isset( $current_user->id ) ? $current_user->id : 0;
		$wp_query->set( 'author', $user_id );

		return $wp_query;
	}//end view_only_users_media

	/**
	 * Filters whether or not the event cost section should be displayed
	 */
	public function maybe_hide_event_cost() {
		return ! current_user_can( 'edit_event_tickets' );
	}
}//end class
