if ( 'undefined' === typeof ajaxurl ) {
	var ajaxurl = TribeTickets.ajaxurl;
}

var tribe_events_community_tickets = {};

( function( $, my ) {
	'use strict';

	my.init = function() {
		$( '.wp-list-table' ).wrap( '<div class="tribe-scrollable-table"/>' );
	};

	$( function() {
		my.init();
	} );
} )( jQuery, tribe_events_community_tickets );
