<?php


/**
 *  CMB2 Post
 *
 */
add_action('cmb2_admin_init', function () {

    $prefix = '_post_';

    $cmb = new_cmb2_box(array(
        'id'            => 'post_metabox',
        'title'         => __('Slider and Link Text', 'cmb2'),
        'object_types'  => array( 'post','page','event' ), // Post type
        'context'       => 'normal',
        'priority'      => 'low',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles'    => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
    ));

    $cmb->add_field(array(
        'name'    => __('Link Text', 'cmb2'),
        'desc'    => __('Custom text for "Read More" buttons', 'cmb2'),
        'default' => 'Read More',
        'id'      => $prefix . 'link_text',
        'type'    => 'text',
    ));

    $cmb->add_field(array(
        'name' => __('Excerpt', 'cmb2'),
        'desc' => __('Custom excerpts for slider, archive / blog pages', 'cmb2'),
        'id'   => $prefix . 'excerpt',
        'type' => 'text',
        'sanitization_cb' => false,
        'escape_cb'       => false,
    ));

    $cmb->add_field(array(
        'name' => __('Excerpt Title', 'cmb2'),
        'desc' => __('Defaults to post heading', 'cmb2'),
        'id'   => $prefix . 'excerpt_heading',
        'type' => 'text',
    ));

    $cmb->add_field(array(
      'name'           => 'Show posts of category',
      'desc'           => 'If a category is selected, posts of that category will be additionally rendered',
      'id'            => $prefix . 'more_posts',
      'type'          => 'select',
      // Use a callback to avoid performance hits on pages where this field is not displayed (including the front-end).
      'default'       => 'None',
      'show_option_none' => true,
      'options_cb'    => '\Spnzr\cmb2_get_term_options',
      // Same arguments you would pass to `get_terms`.
      'get_terms_args' => array(
        'taxonomy'    => 'category',
        'hide_empty'  => false,
      ),
    ));

    $cmb->add_field(array(
      'name'           => 'Show events of category',
      'desc'           => 'If a category is selected, one event will be additionally rendered',
      'id'            => $prefix . 'more_events',
      'type'          => 'select',
      // Use a callback to avoid performance hits on pages where this field is not displayed (including the front-end).
      'default'       => 'None',
      'show_option_none' => true,
      'options_cb'    => '\Spnzr\cmb2_get_term_options',
      // Same arguments you would pass to `get_terms`.
      'get_terms_args' => array(
        'taxonomy'    => 'tribe_events_cat',
        'hide_empty'  => false,
      ),
    ));
});
/**
 *  CMB2  Homepage
 *
 */
add_action('cmb2_admin_init', function () {

    $prefix = '_homepage_';

    $cmb = new_cmb2_box(array(
        'id'            => 'homepage_metabox',
        'title'         => __('Homepage Slider', 'cmb2'),
        'object_types'  => array( 'page' ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_on'       => array( 'key' => 'id', 'value' => array( 16862 ) ),
        // 'show_on'       => array( 'key' => 'front-page', 'value' => '' ),
    ));

    $cmb->add_field(array(
        'name'    => 'Slider',
        'desc'    => 'Turn on a slider top of the page',
        'id'      => $prefix . 'slider_status',
        'type'    => 'radio_inline',
        'default' => false,
        'options' => array(
            true    => __('Active', 'cmb2'),
            false   => __('Inactive', 'cmb2'),
        ),
    ));

    $cmb->add_field(array(
        'name'        => __('Slider Posts'),
        'id'          => $prefix . 'slider_posts',
        'desc'        => 'The attached posts will show in the slider',
        'type'        => 'custom_attached_posts',
        'options' => array(
          'show_thumbnails' => true, // Show thumbnails on the left
          'filter_boxes'    => true, // Show a text box for filtering the results
          'query_args'      => array(
            'posts_per_page' => 50,
            'post_type'      => array( 'page', 'post', 'tribe_events' )
          ), // override the get_posts args
        ),
    ));
});
