<?php

namespace myd;


// Filter Yoast Meta Priority to be low
add_filter('wpseo_metabox_prio', function () {
    return 'low';
});


/**
 * make mailchimp bootstrap-y
 */
add_filter('mc4wp_form_css_classes', function ($classes) {
    $classes[] = 'inline-form';
    return $classes;
});

