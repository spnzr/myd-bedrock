<?php

namespace myd;

add_theme_support('woocommerce');
remove_action('admin_notices', 'woothemes_updater_notice');

add_image_size('card-image', 296, 171);

