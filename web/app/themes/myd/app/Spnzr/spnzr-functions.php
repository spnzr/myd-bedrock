<?php

namespace Spnzr;

/* Remove yoast license warning
 * 
 */
function remove_yoast_license_nag_from_admin_page()
{
    echo
    '<style>
        div.yoast-notice-error {
            display: none;
        }
    </style>';
}

add_action('admin_head', 'Spnzr\\remove_yoast_license_nag_from_admin_page');


function the_attachment_image($size = 'large')
{
    echo wp_get_attachment_image(get_the_ID(), $size);
}


/*
 * returns one term
 */

function nya_title($post_id = false, $taxonomy = 'category')
{
    if (is_category()) {
        return single_term_title('', false);
    } elseif (is_front_page()) {
        return false;
    } elseif (is_author()) {
        return get_the_author_meta('display_name');
    } elseif (is_singular('post')) {
        $terms = get_the_terms($post_id, $taxonomy);
        return esc_attr($terms[0]->name);
    } else {
        return get_the_title();
    }
}


function get_first_term($post_id = false, $taxonomy = 'category')
{
    if ($post_id == false) {
        $post_id = get_the_ID();
    }
    $terms = get_the_terms($post_id, $taxonomy);
    return esc_attr($terms[0]->name);
}


function the_first_term($post_id = false, $taxonomy = 'category')
{
    if ($post_id == false) {
        $post_id = get_the_ID();
    }
    $terms = get_the_terms($post_id, $taxonomy);
    echo esc_attr($terms[0]->name);
}

function the_first_term_link($post_id = false, $taxonomy = 'category')
{
    if ($post_id == false) {
        $post_id = get_the_ID();
    }
    $terms = get_the_terms($post_id, $taxonomy);
    $term = $terms[0];
    if ($term->parent != 0) {
        echo '<a href="' . esc_url(get_term_link($term->term_id)). '" class="link-category" title="' . esc_attr($term->name) . '" ' . '>' . esc_attr($term->name) .'</a> ';
    }
}
/**
 * Renders a really basic nav for WordPress pages
 *
 * @param  $theme_location string
 * @return name string
 *
 */
function get_nav_name($theme_location)
{
    $theme_locations = get_nav_menu_locations();
    $menu_obj = get_term($theme_locations[ $theme_location ], 'nav_menu');
    if (is_string($menu_obj->name)) {
        return $menu_obj->name;
    } else {
        return false;
    }
}
/**
 * Renders a really basic nav for WordPress pages
 *
 * @param  $parent_id int
 * @return echos html
 *
 */
function page_nav($parent_id = 0)
{
/*  post has parents */
  // if( is_page() && $post->post_parent > 0 ) {
  //   $parent_id = $post->post_parent;
  // } else {

  // }

    $children = get_children(array(
      'post_parent' => $parent_id,
      'post_type'   => 'any',
      'numberposts' => -1,
      'post_status' => 'published',
      'orderby'     => 'post_title',
      'order'       => 'ASC'
    ));
    echo "<nav class='pages-nav'>";
    foreach ($children as $child) {
        echo "<a href='". get_permalink($child->ID) . "'>" . get_the_title($child->ID) . "</a>";
    }
    echo "</nav>";
}


/**
 * Renders a WordPress menu from WordPress shortcode
 *
 * https://wordpress.org/plugins/menu-shortcode/
 */
add_shortcode("listmenu", function ($atts, $content = null) {
    extract(shortcode_atts(
        array(
        'menu'            => '',
        'container'       => 'div',
        'container_class' => '',
        'container_id'    => '',
        'menu_class'      => 'menu',
        'menu_id'         => '',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'depth'           => 0,
        'walker'          => '',
        'theme_location'  => ''),
        $atts
    ));
 
 
    return wp_nav_menu(array(
    'menu'            => $menu,
    'container'       => $container,
    'container_class' => $container_class,
    'container_id'    => $container_id,
    'menu_class'      => $menu_class,
    'menu_id'         => $menu_id,
    'echo'            => false,
    'fallback_cb'     => $fallback_cb,
    'before'          => $before,
    'after'           => $after,
    'link_before'     => $link_before,
    'link_after'      => $link_after,
    'depth'           => $depth,
    'walker'          => $walker,
    'theme_location'  => $theme_location));
});

/**
 * Gets a number of terms and displays them as options
 * @param  CMB2_Field $field
 * @return array An array of options that matches the CMB2 options array
 * https://github.com/WebDevStudios/CMB2/wiki/Tips-&-Tricks#a-dropdown-for-taxonomy-terms-which-does-not-set-the-term-on-the-post
 */
function cmb2_get_term_options($field)
{
    $args = $field->args('get_terms_args');
    $args = is_array($args) ? $args : array();

    $args = wp_parse_args($args, array( 'taxonomy' => 'category' ));

    $taxonomy = $args['taxonomy'];

    $terms = (array) cmb2_utils()->wp_at_least('4.5.0')
    ? get_terms($args)
    : get_terms($taxonomy, $args);

  // Initate an empty array
    $term_options = array();
    if (! empty($terms)) {
        foreach ($terms as $term) {
            $term_options[ $term->term_id ] = $term->name;
        }
    }

    return $term_options;
}

/**
 * Include metabox on front page
 * @author Ed Townend
 * @link https://github.com/WebDevStudios/CMB2/wiki/Adding-your-own-show_on-filters
 *
 * @param bool $display
 * @param array $meta_box
 * @return bool display metabox
 */
function ed_metabox_include_front_page($display, $meta_box)
{
    if (! isset($meta_box['show_on']['key'])) {
        return $display;
    }

    if ('front-page' !== $meta_box['show_on']['key']) {
        return $display;
    }

    $post_id = 0;

  // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }

    if (! $post_id) {
        return false;
    }

  // Get ID of page set as front page, 0 if there isn't one
    $front_page = get_option('page_on_front');

  // there is a front page set and we're on it!
    return $post_id == $front_page;
}
add_filter('cmb2_show_on', 'Spnzr\\ed_metabox_include_front_page', 10, 2);
