<?php

namespace Spnzr\User;

/**
 * Making WordPress better for user profiles & Discourse
 *
 *
 */


/**
 * cmb2 metabox
 * add user fields
 * to all user profiles
 */

add_action('cmb2_admin_init', function () {
  $prefix = 'myd_user_';
  /**
   * Metabox for the user profile screen
   */
  $cmb_user = new_cmb2_box(array(
  'id'               => $prefix . 'edit',
  'object_types'     => array( 'user' ), // Tells CMB2 to use user_meta vs post_meta
  // 'show_names'       => true,
  // 'new_user_section' => 'add-existing-user', // where form will show on new user page. 'add-existing-user' & add-new-user only valid options
  ));
  $cmb_user->add_field(array(
  'name'     => __('Title', 'cmb2'),
  'desc'     => __('title (optional)', 'cmb2'),
  'id'       => $prefix . 'title',
  'type'     => 'text',
  ));
  $cmb_user->add_field(array(
    'name'    => __('Photo', 'cmb2'),
    'desc'    => __('profile photo', 'cmb2'),
    'id'      => $prefix . 'photo',
    'type'    => 'file',
    'options' => array(
      'url' => false, // Hide the text input for the url
    ),
  ));
  $currentUser = wp_get_current_user();
  if (in_array('administrator', $currentUser->roles)){
    $cmb_user->add_field(array(
        'name'           => 'Edit Category',
        'desc'           => 'If a category is selected, user can edit it',
        'id'            => $prefix . 'editor',
        'type'          => 'select',
        // Use a callback to avoid performance hits on pages where this field is not displayed (including the front-end).
        'default'       => 'None',
        'show_option_none' => true,
        'options_cb'    => '\Spnzr\cmb2_get_term_options',
        // Same arguments you would pass to `get_terms`.
        'get_terms_args' => array(
          'taxonomy'    => 'category',
          'hide_empty'  => false,
        ),
      ));
  }
});

add_action('init', function () {
  global $wp_roles;

  if ( ! isset( $wp_roles ) )
      $wp_roles = new WP_Roles();

  //You can replace "administrator" with any other role "editor", "author", "contributor" or "subscriber"...
  $wp_roles->roles['subscriber']['name'] = 'Member';
  $wp_roles->role_names['subscriber'] = 'Member';
  $wp_roles->roles['author']['name'] = 'Taskforce Editor';
  $wp_roles->role_names['author'] = 'Taskforce Editor';        
});

/**
* Exclude categories which arent selected for this user.
*/
add_filter( 'get_terms_args', function( $args ) {
  // Dont worry if we're not in the admin screen
  if (! is_admin() )
    return $args;
  // Admin users are exempt.
  $currentUser = wp_get_current_user();
  if (in_array('administrator', $currentUser->roles))
    return $args;

  $include = get_user_meta( $currentUser->ID, 'myd_user_editor', true);

  $args['include'] = $include;
  return $args;
});
/*
Change default category to his/her category !
*/
add_filter('pre_option_default_category', function($ID) {
  // Avoid error or heavy load !
  if ( ! is_user_logged_in() )
    return $ID;
  $user_id = get_current_user_id();
  $restrict_cat = get_user_meta( $user_id, 'myd_user_editor', true);
  if ( is_array($restrict_cat) ) {
    return reset($restrict_cat);
  } else {
    return $restrict_cat;
  }
});

/**
  * on user profiles
  * hide all the stupid fields
  */
remove_action('admin_color_scheme_picker', 'admin_color_scheme_picker');

if (! function_exists(__NAMESPACE__ . '\\cor_remove_personal_options')) {
  /**
   * Removes the leftover 'Visual Editor', 'Keyboard Shortcuts' and 'Toolbar' options.
   */
    function cor_remove_personal_options($subject)
    {
        $subject = preg_replace('#<h2>Personal Options</h2>.+?/table>#s', '', $subject, 1);
        $subject = preg_replace('#<tr class="user-first-name-wrap">.+?/tr>#s', '', $subject, 1);
        $subject = preg_replace('#<tr class="user-last-name-wrap">.+?/tr>#s', '', $subject, 1);
        $subject = preg_replace('#<tr class="user-display-name-wrap">.+?/tr>#s', '', $subject, 1);
        $subject = preg_replace('#Nickname#s', 'Name', $subject, 1);
        // $subject = preg_replace( '#<tr class="user-profile-picture">.+?/tr>#s', '', $subject, 1 );
        $subject = preg_replace('#<h2>Account Management</h2>.+?/table>#s', '', $subject, 1);
        return $subject;
    }

    function cor_profile_subject_start()
    {
        ob_start(__NAMESPACE__ . '\\cor_remove_personal_options');
    }

    function cor_profile_subject_end()
    {
        ob_end_flush();
    }
}
add_action('admin_head-profile.php', __NAMESPACE__ . '\\cor_profile_subject_start');
add_action('admin_footer-profile.php', __NAMESPACE__ . '\\cor_profile_subject_end');

add_filter('pre_option_avatar_default', function () {
    // this assumes default_avatar.png is in wp-content/themes/active-theme/images
    return get_bloginfo('template_directory') . '/images/default_avatar.png';
});




/**
  * avatar_override()
  *
  * Overrides an avatar with a profile image
  *
  * @param string $avatar SRC to the avatar
  * @param mixed $id_or_email
  * @param int $size Size of the image
  * @param string $default URL to the default image
  * @param string $alt Alternative text
  **/


  add_filter('get_avatar', '\Spnzr\User\avatar_override', 10, 6);

function avatar_override($avatar, $id_or_email, $size, $default, $alt, $args = array())
{
    global $pagenow;
    if ('options-discussion.php' == $pagenow) {
        return $avatar; //Stop overriding gravatars on options-discussion page
    }
    
    //Get user data
    if (is_numeric($id_or_email)) {
        $user = get_user_by('id', ( int )$id_or_email);
    } elseif (is_object($id_or_email)) {
        $comment = $id_or_email;
        if (empty($comment->userID)) {
            $user = get_user_by('id', $comment->userID);
        } else {
            $user = get_user_by('email', $comment->comment_author_email);
        }
        if (!$user) {
            return $avatar;
        }
    } elseif (is_string($id_or_email)) {
        $user = get_user_by('email', $id_or_email);
    } else {
        return $avatar;
    }
    if (!$user) {
        return $avatar;
    }
    $userID = $user->ID;

    $userPhoto  = get_user_meta($userID, 'myd_user_photo_id', true);
    $userPhoto = wp_get_attachment_image($userPhoto, array(40,40));

    if (! $userPhoto) {
        return $avatar;
    }
    return $userPhoto;
} //end avatar_override



/**
 * cmb2 metabox
 * to attach user IDs
 * to page id 80 aka leadership
 */
add_action('cmb2_admin_init', function () {

    $prefix = '_show_profiles_';

    $cmb = new_cmb2_box(array(
      'id'            => 'user_profile_metabox',
      'title'         => __('Display User Profiles', 'cmb2'),
      'object_types'  => array( 'page' ), // Post type
      'context'       => 'normal',
      'priority'      => 'high',
      'show_on'       => array( 'key' => 'id', 'value' => array( 80 ) ),
      'show_names'    => true, // Show field names on the left
      // 'cmb_styles'    => false, // false to disable the CMB stylesheet
      // 'closed'     => true, // Keep the metabox closed by default
    ));

    $group_field_id = $cmb->add_field(array(
    'id'          => $prefix . 'id_group',
    'type'        => 'group',
    'description' => __('Create groups of user profiles', 'cmb2'),
    // 'repeatable'  => false, // use false if you want non-repeatable group
    'options'     => array(
      'group_title'   => __('Entry {#}', 'cmb2'), // since version 1.1.4, {#} gets replaced by row number
      'add_button'    => __('Add Another User Group', 'cmb2'),
      'remove_button' => __('Remove User Group', 'cmb2'),
      'sortable'      => true, // beta
      // 'closed'     => true, // true to have the groups closed by default
    ),
    ));
  // Id's for group's fields only need to be unique for the group. Prefix is not needed.
    $cmb->add_group_field($group_field_id, array(
    'name'    => 'User group title',
    'id'      => 'title',
    'type'    => 'text',
    ));
    $cmb->add_group_field($group_field_id, array(
    'name'    => 'User',
    'id'      => 'userID',
    'type'    => 'custom_attached_posts',
    'options' => array(
      'number' => '5',
      'show_thumbnails' => true, // Show thumbnails on the left
      'filter_boxes'    => true, // Show a text box for filtering the results
      'query_users'     => true, // Do users instead of posts/custom-post-types.
    ),
    ));
});
