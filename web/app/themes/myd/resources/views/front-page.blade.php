@extends('layouts.base')

@section('content')


<article @php(post_class())>


  <header id="front-page-content"> 

  	<section class="center-700">
		<?php
        while (have_posts()) : the_post();
            the_content();
        endwhile;
        ?>
    </div>

    <section id="front-events" class="row">
        <img class="half hidden-sm-down" src="@asset('images/marching-bw.jpg')" width="100%" height="100%" alt="fun young democrats event"/>
        <div class="events half sm-full-width">
            <h2><a href="/events/">Attend an Event</a></h2>
            @php(
                $events = tribe_get_events(array(
                    'posts_per_page' => 3,
                    'start_date' => date( 'Y-m-d H:i:s' )
                ))
            )
            @foreach ($events as $event)
                @include('partials.event-item')
            @endforeach
        </div>
    </section>



    <div id="front-news">
        <h2>News &amp; Updates</h2>
        @include('views/partials/content-cards')
    </div>

    <section id="front-member" class="row">
        <div class="half sm-full-width">
            <h2><a href="/member-of-the-month/">Member of the Month</a></h2>
            <div class="card">
                <div class="card-block">
            <?php
                $MemberQuery = new WP_Query(array(
                    'category_name'                     => 'member-of-the-month',
                    'post_type'                         => 'post' ,
                    'posts_per_page'                    => 1 ,
                    'no_found_rows'                     => true ,  // turn off pagination information
                    'update_post_meta_cache'            => false ,  // don't do anything with post meta cache
                ));
                if ($MemberQuery) :
                while ($MemberQuery->have_posts()) : $MemberQuery->the_post();
                    $excerpt   = get_post_meta(get_the_ID(), '_post_excerpt', true);
                    $link_text = get_post_meta(get_the_ID(), '_post_link_text', true);
                    if (empty($excerpt)) {
                        $excerpt = get_the_excerpt();
                    }
                    if (empty($link_text)) {
                        $link_text = 'Read More';
                    }
                ?>
                    <div class="row">
                        <div class="half">{!! the_post_thumbnail('medium',array('class' => 'img-fluid img-thumbnail')) !!}</div>
                        <div class="half">
                            <h3><a href="{{ get_permalink() }}">{{ get_the_title() }}</a></h3>
                            <p><?php echo wp_trim_words(get_the_excerpt(), 35, '...'); ?></p>
                            <p><a class="btn btn-success excerpt-more" href="/member-of-the-month/">{{ $link_text }}</a></p>
                        </div>
                    </div>
            <?php
                endwhile;
                endif;
                wp_reset_postdata();
            ?>
                </div>
            </div>
        </div>
        <div class="half sm-full-width">
            <h2><a href="/taskforces">MYD Taskforces</a></h2>
            <div class="card">
                <div class="card-block">
                    <p>Our taskforces are member-driven forums to engage in, create a dialogue, and plan for specific actions.</p>
                    <p>Get involved by coming to a meeting!</p>
                <?php echo Spnzr\page_nav(17511); ?>
            </div>
          </div>
        </div>
    </section>

</article>

@endsection

