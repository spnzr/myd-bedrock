<?php
/**
 * Template part to create cards if post meta has category
 * @param post_meta _post_more_posts
 */


$more_posts      = get_post_meta(get_the_ID(), '_post_more_posts', true);
if ($more_posts):


if (get_page_template_slug() == 'template-one-one-three.php') {
  $offset = 1;
} else { 
  $offset = 0;
}
?>
    

<div class="card-deck">
<?php
  $NewQuery = new WP_Query(array(
    'cat' => $more_posts ,
    'posts_per_page' => 3 ,
    'post_type' => 'post' ,
    'no_found_rows' => true ,  // turn off pagination information
    'update_post_meta_cache' => false ,  // don't do anything with post meta cache
    'orderby' => 'post_date',
    'order' => 'DESC',
    'offset' => $offset

    ));
  if ($NewQuery) :
  while ($NewQuery->have_posts()) : $NewQuery->the_post();
    $excerpt          = get_post_meta(get_the_ID(), '_post_excerpt', true);
    $excerpt_heading  = get_post_meta(get_the_ID(), '_post_excerpt_heading', true);
    $link_text        = get_post_meta(get_the_ID(), '_post_link_text', true);

    // sanatize excerpt
    if (empty($excerpt)) {
      $excerpt = get_the_excerpt();
    }
    if (empty($excerpt_heading)) {
      $excerpt_heading = get_the_title();
    }
    if (empty($link_text)) {
      $link_text = 'Read More';
    }
?>
  <section class="card">
    <figure class="card-img-top">
      <?php the_post_thumbnail('card-image'); ?>
    </figure>
    <div class="card-block">
      <h3 class="card-title">
        <a href="<?php echo get_permalink($post->ID); ?>"><?= $excerpt_heading; ?></a>
      </h3>
      <p><?= $excerpt; ?></p>
      <p><a class="btn btn-success excerpt-more" href="<?= get_permalink(); ?>"><?= esc_html($link_text); ?></a></p>
    </div>
  </section>
<?php
  endwhile;
  endif;
?>
</div>

<?php if ($NewQuery->found_posts > 3) : ?>
  <a class="btn btn-primary" href="<?= esc_url(get_category_link($more_posts)); ?>">See more <?= get_the_category_by_ID($more_posts) ?></a>
<?php endif; ?>

<?php wp_reset_postdata(); ?>
<? endif; ?>
