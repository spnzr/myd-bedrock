<div class="event-item">
    <a href="{{tribe_get_event_link($event->ID)}}">
        <p class="event-date">{{tribe_get_start_date($event->ID,true)}}</p>
        @if ( tribe_get_venue($event->ID) ) <p class="event-location">{{ tribe_get_venue($event->ID) }}, {{ tribe_get_address($event->ID) }}</p> @endif
        <h3 class="event-title">{{$event->post_title}}</h3>
        <p class="event-tagline">{{$event->post_content}}</p>
    </a>
</div>