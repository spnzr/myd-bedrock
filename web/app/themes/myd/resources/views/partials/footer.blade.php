<footer class="content-info">
 	<div class="container-fluid">
		<nav id="nav-footer" class="row">
  		<?php
          if (has_nav_menu('connect_navigation')) :
            echo '<div class="quarter">';
            echo '<h4>' . wp_get_nav_menu_object('connect')->name . '</h4>';
            wp_nav_menu(['theme_location' => 'connect_navigation', 'menu_class' => 'list-unstyled']);
            echo '</div>';
          endif;
          ?>
          <?php
          if (has_nav_menu('learn_navigation')) :
            echo '<div class="quarter">';
            echo '<h4>' . wp_get_nav_menu_object('learn')->name . '</h4>';
            wp_nav_menu(['theme_location' => 'learn_navigation', 'menu_class' => 'list-unstyled']);
            echo '</div>';
          endif;
          ?>
          <?php
          if (has_nav_menu('engage_navigation')) :
            echo '<div class="quarter">';
            echo '<h4>' . wp_get_nav_menu_object('engage')->name . '</h4>';
            wp_nav_menu(['theme_location' => 'engage_navigation', 'menu_class' => 'list-unstyled']);
            echo '</div>';
          endif;
          ?>
          <?php
          if (has_nav_menu('members_navigation')) :
            echo '<div class="quarter">';
            echo '<h4>' . wp_get_nav_menu_object('members')->name . '</h4>';
            wp_nav_menu(['theme_location' => 'members_navigation', 'menu_class' => 'list-unstyled']);
            echo '</div>';
          endif;
          ?>
        </nav>
    </div>
    <div class="paid-for">Paid For by MYD Members</div>
    <?php dynamic_sidebar('sidebar-footer'); ?>
    </div>
</footer>
