<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  @php(wp_head())
  <meta name="google-site-verification" content="f7ClM65UnEAM7V4WzUb3aDnMFHSuJhDMJXWw1s3D47U" />
</head>
