<header class="banner" id="pageHeader">
  <nav id="header-navbar" class="navbar navbar-toggleable-sm navbar-light">
    <button class="navbar-toggler navbar-toggler-right collapsed" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fa fa-2x fa-bars" aria-hidden="true"></i>
    </button>
    <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>">
      <h2 class="brand-title">@php( bloginfo('name'))</h2>
    </a>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu([
          'theme_location'  => 'primary_navigation',
          'menu_id'         => false,
          'menu_class'      => 'navbar-nav nav',
          'fallback_cb'     => 'bs4navwalker::fallback',
          'walker'          => new bs4navwalker()
        ]);
      endif;
    ?>
      <div class="d-sm-block d-none ml-auto">
        @include('partials.social-links')
      </div>
    </div>
  </nav>
@include('partials.slider-slide')
</header>

