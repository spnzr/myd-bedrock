
<div class="page-header">
  <h1>{{ App\title() }}</h1>

<?php 
  $more_posts     = get_post_meta(get_the_ID(), '_post_more_events', true);
  if($more_posts == 0){
    return;
  }
  $events         = tribe_get_events(array(
    'posts_per_page' => 1 ,
    'no_found_rows' => true ,  // turn off pagination information
    'update_post_meta_cache' => false ,  // don't do anything with post meta cache
    'tax_query'=> array(
      array(
        'taxonomy' => 'tribe_events_cat',
        'terms'    => $more_posts
    ))
  ));
  if (!$events){
    $events = tribe_get_events(array(
    'posts_per_page' => 1,
    'start_date' => date( 'Y-m-d H:i:s' )
    ));
  }
  foreach ($events as $event):
?>
  @include('partials.event-item')
@endforeach

</div>
