<h3>Find Your Election District</h3>
<div id="iframePollSite">
	<iframe src="https://nyc.pollsitelocator.com/search" width="380" height="800" class="iframe"></iframe>
</div>

<section class="widget">
	<h3>Have questions? <small>- <a href="/about/contact/">Contact Us</a></small></h3>
	Or email our <a href="mailto:openseat@gomyd.com">Open Seat Director, Jamie</a> 
	<?php get_template_part('templates/social-links') ?>
</section>