<section class="widget">
  <h3>Connect <small>- <a href="/about/contact/">Contact Us</a></small></h3>
  @include('partials.social-links')
</section>

<section class="widget">
  @include('partials.search')
</section>

<section class="widget">
  <h3>Attend an Event</h3>
  @php(
    $events = tribe_get_events(array(
        'posts_per_page' => 3,
        'start_date' => date( 'Y-m-d H:i:s' )
    ))
  )
  @foreach ($events as $event)
      @include('partials.event-item')
  @endforeach
</section>

@php( dynamic_sidebar('sidebar-primary'))
