
<?php

$sliderstatus   = get_post_meta(get_the_ID(), '_homepage_slider_status', true);

if ($sliderstatus) :
    $slider_posts   = get_post_meta(get_the_ID(), '_homepage_slider_posts', true);
    // $slider_posts	= explode(',' , $slider_posts);

?>

<div class="flexslider hero-flex">
    <section class="slides">
    <?php
    foreach ($slider_posts as $slide_post_id) :
        $slide_post_id      = trim($slide_post_id);
        $excerpt            = get_post_meta($slide_post_id, '_post_excerpt', true);
        $excerpt_heading    = get_post_meta($slide_post_id, '_post_excerpt_heading', true);
        $link_text          = get_post_meta($slide_post_id, '_post_link_text', true);
        $bg_image           = wp_get_attachment_url(get_post_thumbnail_id($slide_post_id));

        // sanatize excerpt
        if (empty($excerpt)) {
            $excerpt = ' ';
        }
        if (empty($excerpt_heading)) {
            $excerpt_heading = get_the_title($slide_post_id);
        }
        // sanatize link text
        if (empty($link_text)) {
            $link_text = 'Read More';
        }
    ?>
        <div class="slide" id="slide-<?= $slide_post_id; ?>" <? if($bg_image): ?> style="background:url(<?= $bg_image ?>) no-repeat center; background-size: cover;" <? endif; ?>>
            <a class="" target="_blank" href="<?= get_permalink($slide_post_id); ?>">
                <div class="hero-bottom">
                  <h2 id="hero-<?= $slide_post_id; ?>" class="hero-title"><?= $excerpt_heading ?></h2>
                  <div class="hero-text animate-words"> <?= $excerpt ?> </div>
                  <button class="btn btn-success hero-btn"><?= esc_html($link_text) ?></button>
                </div>
            </a>
        </div>
    <?php endforeach; ?>
    </section>    
</div>


<?php
endif;

?>