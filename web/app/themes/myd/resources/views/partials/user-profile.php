<?php
/**
 * User profile template
 * @param userID
 */

  $userMeta   = array_map(function ($a) {
  return $a[0]; }, get_user_meta($userID));
  $name       = get_user_meta($userID, 'nickname', true);
  $nameLink   = preg_replace('/[^A-Za-z0-9\-]/', '', $name);
  $bio        = get_user_meta($userID, 'description', true);
  $user       = get_userdata($userID)->user_login;
  $email      = get_userdata($userID)->user_email;
  $url        = get_user_meta($userID, 'user_url', true);
  $userTitle  = get_user_meta($userID, 'myd_user_title', true);
  $userPhoto  = get_user_meta($userID, 'myd_user_photo_id', true);
?>

<div class="user-profile">
    <?php if ($bio) : ?>
        <a href="#modal-bio" data-toggle="modal" data-target="#modal-bio-<?= $nameLink ?>">
    <?php endif; ?>
  <?php echo wp_get_attachment_image($userPhoto, 'thumbnail', false, array('class' => 'img-fluid img-thumbnail')); ?>
  <?php if ($bio) : ?>
        </a>
    <?php endif; ?>
  <h3><?= $name ?></h3>
  <p><?= $userTitle ?></p>
  <p><a href="mailto:<?= $email ?>"><?= $email ?></a></p>
  <?php
        if ($url || $bio) {
            echo "<p>";
        }
        if ($url) {
            echo "&sim; <a href='" . $url . "'>" . $url . "</a> ";
        }
        if ($bio) : ?>
            &sim;   <a href="#modal-bio" data-toggle="modal" data-target="#modal-bio-<?= $nameLink ?>">bio</a> &sim;
    <?php endif; ?>
    <?php if ($url || $bio) : ?>
        </p>
    <?php endif; ?>
</div>

<?php	if ($bio) : ?>
      <div class="modal fade" id="modal-bio-<?= $nameLink ?>">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                    <h5 class="modal-title"><?= $name ?> <br><small><?= $userTitle ?></small></h5>
                </div>
            <div class="modal-body">
                    <p><?= $bio ?></p>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-secondary"><a href="mailto:<?= $email ?>">Email</a></button>
              </div>
            </div>
        </div>
      </div>
<?php endif; ?>