{{-- 
    Template Name: No Sidebar Template 
--}}

@extends('layouts.base')

@section('content')
  <div class="center-700">	
  @while(have_posts()) @php(the_post())
    @include('partials.page-header')
    @include('partials.content-page')
  @endwhile
  </div>
@endsection