{{--
    Template Name: Page, one post, three cards 
--}}

@extends('layouts.base')

@section('content')
  @while(have_posts()) @php(the_post())
    @include('partials.content-page')
  @endwhile

  <div class="py-2">
    <?php
      $more_posts     = get_post_meta(get_the_ID(), '_post_more_posts', true);
      $NewQuery       = new WP_Query(array(
        'cat'                     => $more_posts ,
        'posts_per_page'          => 1 ,
        'post_type'               => 'post' ,
        'no_found_rows'           => true ,  // turn off pagination information
        'update_post_meta_cache'  => false ,  // don't do anything with post meta cache
        'orderby'                 => 'date',
        'order'                   => 'DESC',
        ));
      if ($NewQuery) :
      while ($NewQuery->have_posts()) : $NewQuery->the_post();
        get_template_part('templates/page', 'header');
        the_post_thumbnail('medium');
        get_template_part('templates/content', 'page');
      endwhile;
      endif;
      wp_reset_postdata();
    ?>
    @include('partials.social-links-share')
  </div>
  @include('partials.content-cards')

@endsection