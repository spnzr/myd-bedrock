{{--
    Template Name: Open Seat
--}}

@extends('layouts.base')

@section('content')
  @while(have_posts()) @php(the_post())
    @include('partials.page-header')
    @if (has_nav_menu('open_seat_navigation'))
      {!! wp_nav_menu(['theme_location' => 'open_seat_navigation','menu_class' => 'nav navbar-nav h6']) !!}
    @endif
    @include('partials.content', 'page'); ?>
  @endwhile
@endsection