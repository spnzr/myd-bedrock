{{--
    Template Name: User Template 
--}}

@extends('layouts.base')

@section('content')

<div class="center-700">	
@while(have_posts()) @php(the_post())
  @include('partials.page-header')
  @include('partials.content-page')
@endwhile
  
<?php
  $users = get_post_meta(get_the_ID(), '_show_profiles_id_group', true);
  foreach ((array) $users as $key => $entry) {
    if (isset($entry['title'])) {
      echo "<h2>" . esc_html($entry['title']) . "</h2><div class='user-profiles'>";
    }
    foreach ($entry['userID'] as $userID) {
      set_query_var('userID', absint($userID));
      get_template_part('partials/user-profile');
    }
    echo "</div>";
  }
?>
</div>
@endsection
