<?php
/**
 * Single Event Meta Template
 *
 * Override this template in your own theme by creating a file at:
 * [your-theme]/tribe-events/modules/meta.php
 *
 * @package TribeEventsCalendar
 */

	/**
	 * spnzr
	 * cleaned up template 
	 * deleted a lot
	 */

do_action( 'tribe_events_single_meta_before' );

?>

<div class="row mb-4">

<?php
do_action( 'tribe_events_single_event_meta_primary_section_start' );

tribe_get_template_part( 'modules/meta/details' );

tribe_get_template_part( 'modules/meta/venue' );

if ( tribe_has_organizer() ) {
	tribe_get_template_part( 'modules/meta/organizer' );
}

if ( tribe_embed_google_map() ) {
	echo '<div class="tribe-events-meta-group col-8 tribe-events-meta-group-gmap">';
	tribe_get_template_part( 'modules/meta/map' );
	echo '</div>';
}


do_action( 'tribe_events_single_event_meta_primary_section_end' );
?>

</div>

<?php
do_action( 'tribe_events_single_meta_after' );
